package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

public class HourlyData {
    @SerializedName("time")
    private long time;
    @SerializedName("summary")
    private String summary;
    @SerializedName("icon")
    private String icon;
    @SerializedName("precipIntensity")
    private float precipIntensity;
    @SerializedName("precipProbability")
    private float precipProbability;
    @SerializedName("precipType")
    private String precipType;
    @SerializedName("temperature")
    private float temperature;
    @SerializedName("apparentTemperature")
    private float apparentTemperature;
    @SerializedName("dewPoint")
    private float dewPoint;
    @SerializedName("humidity")
    private float humidity;
    @SerializedName("windSpeed")
    private float windSpeed;
    @SerializedName("windBearing")
    private int windBearing;
    @SerializedName("visibility")
    private float visibility;
    @SerializedName("cloudCover")
    private float cloudCover;
    @SerializedName("pressure")
    private float pressure;
    @SerializedName("ozone")
    private float ozone;


    public long getTime() {
        return time;
    }

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public float getPrecipIntensity() {
        return precipIntensity;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public String getPrecipType() {
        return precipType;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getApparentTemperature() {
        return apparentTemperature;
    }

    public float getDewPoint() {
        return dewPoint;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public float getVisibility() {
        return visibility;
    }

    public float getCloudCover() {
        return cloudCover;
    }

    public float getPressure() {
        return pressure;
    }

    public float getOzone() {
        return ozone;
    }

}
