package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Hourly {
    @SerializedName("summary")
    private String summary;
    @SerializedName("icon")
    private String icon;
    @SerializedName("data")
    private ArrayList<HourlyData> data;

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public ArrayList<HourlyData> getData() {
        return data;
    }
}