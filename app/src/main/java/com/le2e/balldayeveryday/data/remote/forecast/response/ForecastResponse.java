package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ForecastResponse {
    @SerializedName("latitude")
    private float latitude;
    @SerializedName("longitude")
    private float longitude;
    @SerializedName("timezone")
    private String timezone;
    @SerializedName("currently")
    private Currently currently;
    @SerializedName("hourly")
    private Hourly hourly;
    @SerializedName("daily")
    private Daily daily;
    @SerializedName("alerts")
    private ArrayList<Alerts> alerts;

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public Currently getCurrently() {
        return currently;
    }

    public Hourly getHourly() {
        return hourly;
    }

    public Daily getDaily() {
        return daily;
    }

    public ArrayList<Alerts> getAlerts() {
        return alerts;
    }
}





