package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

public class MinutelyData {
    @SerializedName("time")
    private long time;
    @SerializedName("precipIntensity")
    private float precipIntensity;
    @SerializedName("precipIntensityError")
    private float precipIntensityError;
    @SerializedName("precipProbability")
    private float precipProbability;
    @SerializedName("precipType")
    private String precipType;

    public long getTime() {
        return time;
    }

    public float getPrecipIntensity() {
        return precipIntensity;
    }

    public float getPrecipIntensityError() {
        return precipIntensityError;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public String getPrecipType() {
        return precipType;
    }
}
