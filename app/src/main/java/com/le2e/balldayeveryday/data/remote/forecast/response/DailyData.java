package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

public class DailyData {
    @SerializedName("time")
    private long time;
    @SerializedName("summary")
    private String summary;
    @SerializedName("icon")
    private String icon;
    @SerializedName("sunriseTime")
    private long sunriseTime;
    @SerializedName("sunsetTime")
    private long sunsetTime;
    @SerializedName("moonPhase")
    private float moonPhase;
    @SerializedName("precipIntensity")
    private float precipIntensity;
    @SerializedName("precipIntensityMax")
    private float precipIntensityMax;
    @SerializedName("precipIntensityMaxTime")
    private long precipIntensityMaxTime;
    @SerializedName("precipProbability")
    private float precipProbability;
    @SerializedName("precipType")
    private String precipType;
    @SerializedName("temperatureMin")
    private float temperatureMin;
    @SerializedName("temperatureMinTime")
    private long temperatureMinTime;
    @SerializedName("temperatureMax")
    private float temperatureMax;
    @SerializedName("temperatureMaxTime")
    private long temperatureMaxTime;
    @SerializedName("apparentTemperatureMin")
    private float apparentTemperatureMin;
    @SerializedName("apparentTemperatureMinTime")
    private long apparentTemperatureMinTime;
    @SerializedName("apparentTemperatureMax")
    private float apparentTemperatureMax;
    @SerializedName("apparentTemperatureMaxTime")
    private long apparentTemperatureMaxTime;
    @SerializedName("dewPoint")
    private float dewPoint;
    @SerializedName("humidity")
    private float humidity;
    @SerializedName("windSpeed")
    private float windSpeed;
    @SerializedName("windBearing")
    private int windBearing;
    @SerializedName("visibility")
    private float visibility;
    @SerializedName("cloudCover")
    private float cloudCover;
    @SerializedName("pressure")
    private float pressure;
    @SerializedName("ozone")
    private float ozone;

    public long getTime() {
        return time;
    }

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public long getSunriseTime() {
        return sunriseTime;
    }

    public long getSunsetTime() {
        return sunsetTime;
    }

    public float getMoonPhase() {
        return moonPhase;
    }

    public float getPrecipIntensity() {
        return precipIntensity;
    }

    public float getPrecipIntensityMax() {
        return precipIntensityMax;
    }

    public long getPrecipIntensityMaxTime() {
        return precipIntensityMaxTime;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public String getPrecipType() {
        return precipType;
    }

    public float getTemperatureMin() {
        return temperatureMin;
    }

    public long getTemperatureMinTime() {
        return temperatureMinTime;
    }

    public float getTemperatureMax() {
        return temperatureMax;
    }

    public long getTemperatureMaxTime() {
        return temperatureMaxTime;
    }

    public float getApparentTemperatureMin() {
        return apparentTemperatureMin;
    }

    public long getApparentTemperatureMinTime() {
        return apparentTemperatureMinTime;
    }

    public float getApparentTemperatureMax() {
        return apparentTemperatureMax;
    }

    public long getApparentTemperatureMaxTime() {
        return apparentTemperatureMaxTime;
    }

    public float getDewPoint() {
        return dewPoint;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public float getWindBearing() {
        return windBearing;
    }

    public float getVisibility() {
        return visibility;
    }

    public float getCloudCover() {
        return cloudCover;
    }

    public float getPressure() {
        return pressure;
    }

    public float getOzone() {
        return ozone;
    }
}
