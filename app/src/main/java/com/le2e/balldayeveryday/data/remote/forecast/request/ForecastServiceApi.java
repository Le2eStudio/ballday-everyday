package com.le2e.balldayeveryday.data.remote.forecast.request;


import com.le2e.balldayeveryday.data.remote.forecast.response.ForecastResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface ForecastServiceApi {
    @GET("/forecast/{apiKey}/{location}")
    Observable<ForecastResponse> obsGetForecast(@Path("apiKey") String apiKey, @Path("location") String location);
}
