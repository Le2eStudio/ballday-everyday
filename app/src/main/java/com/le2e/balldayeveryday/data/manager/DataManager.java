package com.le2e.balldayeveryday.data.manager;


import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.data.remote.forecast.request.ApiContentHelper;
import com.le2e.balldayeveryday.data.remote.forecast.response.ForecastResponse;

import rx.Observable;
import rx.functions.Func0;

public class DataManager {
    private ApiContentHelper apiContentHelper;
    private PreferencesManager prefManager;

    public DataManager(ApiContentHelper helper, SharedPreferences sharedPreferences) {
        this.apiContentHelper = helper;
        prefManager = new PreferencesManager(sharedPreferences);
    }

    public Observable<ForecastResponse> getForecastFromApi(final String location) {
        return Observable.defer(new Func0<Observable<ForecastResponse>>() {
            @Override
            public Observable<ForecastResponse> call() {
                return apiContentHelper.getForecast(location);
            }
        });
    }

    public void saveLatLng(LatLng latLng) {
        prefManager.saveLatLng(latLng);
    }

    public LatLng getLatLng() {
        return prefManager.getLatLng();
    }
}
