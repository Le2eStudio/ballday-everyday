package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Daily {
    @SerializedName("summary")
    private String summary;
    @SerializedName("icon")
    private String icon;
    @SerializedName("data")
    private ArrayList<DailyData> data;

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public ArrayList<DailyData> getData() {
        return data;
    }
}
