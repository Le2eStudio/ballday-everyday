package com.le2e.balldayeveryday.data.manager;


import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

class PreferencesManager {
    private static final String LONGITUDE_TAG = "longitude";
    private static final String LATITUDE_TAG = "latitude";

    private SharedPreferences sharedPreferences;

    PreferencesManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    void saveLatLng(LatLng latLng) {
        sharedPreferences.edit()
                .putLong(LATITUDE_TAG, Double.doubleToLongBits(latLng.latitude))
                .putLong(LONGITUDE_TAG, Double.doubleToLongBits(latLng.longitude))
                .apply();
    }

    LatLng getLatLng() {
        double lat = Double.longBitsToDouble(
                sharedPreferences.getLong(LATITUDE_TAG, 0));

        double lng = Double.longBitsToDouble(
                sharedPreferences.getLong(LONGITUDE_TAG, 0));

        return new LatLng(lat, lng);
    }
}
