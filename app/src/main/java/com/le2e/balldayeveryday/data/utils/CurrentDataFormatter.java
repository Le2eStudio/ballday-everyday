package com.le2e.balldayeveryday.data.utils;


import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CurrentDataFormatter {

    private SimpleDateFormat defaultFormatter = new SimpleDateFormat("M/dd - h:mm a");

    public String createDateDisplayString(long timeToConvert, @Nullable SimpleDateFormat fmt) {
        if (fmt == null) {
            fmt = defaultFormatter;
        }

        return fmt.format(new Date(timeToConvert * 1000L));
    }

    public String createTempDisplayString(float temperature, float apparentTemperature) {
        return Math.round(temperature) + " / " + Math.round(apparentTemperature);
    }

    public String createPrecipDisplayString(float precipProbability, String precipType, float precipIntensity) {
        int precipChance = Math.round(precipProbability * 100);
        if (precipType != null)
            return precipChance + "% / " + precipType + precipIntensity + "\"";
        else
            return precipChance + "%";
    }

    public String createHumidityDisplayString(float humidity) {
        return Math.round(humidity) + "%";
    }

    public String createWindDisplayString(float windSpeed, float windBearing) {
        if (windSpeed != 0.0f){
            // calculate direction string
            return Math.round(windSpeed) + " mph " + Math.round(windBearing);
        } else
            return Math.round(windSpeed) + " mph";
    }

    public String createPressureDisplayString(float pressure) {
        return String.format(Locale.getDefault(), "%.2f", pressure) + " mBar";
    }

    public String createVisibilityDisplayString(float visibility) {
        return String.format(Locale.getDefault(), "%.3f", visibility) + " miles";
    }

    public String createCloudDisplayString(float cloudCover) {
        return Math.round(cloudCover) + "%";
    }
}
