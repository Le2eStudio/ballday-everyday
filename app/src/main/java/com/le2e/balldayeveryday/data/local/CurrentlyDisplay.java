package com.le2e.balldayeveryday.data.local;


public class CurrentlyDisplay {
    private String time;
    private String summary;
    private String temp;
    private String precip;
    private String humidity;
    private String wind;
    private String pressure;
    private String visibility;
    private String cloud;

    public String getTime() {
        return time;
    }

    public String getSummary() {
        return summary;
    }

    public String getTemp() {
        return temp;
    }

    public String getPrecip(){
        return precip;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getWind() {
        return wind;
    }

    public String getPressure() {
        return pressure;
    }

    public String getVisibility() {
        return visibility;
    }

    public String getCloud() {
        return cloud;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public void setPrecip(String precip){
        this.precip = precip;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public void setCloud(String cloud) {
        this.cloud = cloud;
    }
}
