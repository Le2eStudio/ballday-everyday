package com.le2e.balldayeveryday.data.remote.forecast.request;


import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.BuildConfig;
import com.le2e.balldayeveryday.data.remote.forecast.response.ForecastResponse;

import rx.Observable;

public class ApiContentHelper {
    private ForecastServiceApi api;

    public ApiContentHelper(ForecastServiceApi api){
        this.api = api;
    }

    public Observable<ForecastResponse> getForecast(String location){
        return api.obsGetForecast(BuildConfig.FORECAST_API_KEY, location);
    }

    public Observable<ForecastResponse> getForecast(LatLng location){
        String loc = location.latitude + ", " + location.longitude;
        return api.obsGetForecast(BuildConfig.FORECAST_API_KEY, loc);
    }
}
