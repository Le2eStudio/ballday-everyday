package com.le2e.balldayeveryday.data.remote.forecast.response;

import com.google.gson.annotations.SerializedName;

public class Alerts {
    @SerializedName("title")
    private String title;
    @SerializedName("time")
    private long time;
    @SerializedName("expires")
    private long expires;
    @SerializedName("description")
    private String description;
    @SerializedName("uri")
    private String uri;

    public String getTitle() {
        return title;
    }

    public long getTime() {
        return time;
    }

    public long getExpires() {
        return expires;
    }

    public String getDescription() {
        return description;
    }

    public String getUri() {
        return uri;
    }
}
