package com.le2e.balldayeveryday.injection.module;

import com.le2e.balldayeveryday.BuildConfig;
import com.le2e.balldayeveryday.data.remote.forecast.request.ApiContentHelper;
import com.le2e.balldayeveryday.data.remote.forecast.request.ForecastServiceApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = NetworkModule.class)
public class ForecastModule {
    @Provides
    @Singleton
    ForecastServiceApi getForecastApi(OkHttpClient client){
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(BuildConfig.BASE_URL)
                .build();

        return retrofit.create(ForecastServiceApi.class);
    }

    @Provides
    @Singleton
    ApiContentHelper getApiContentHelper(ForecastServiceApi forecastServiceApi){
        return new ApiContentHelper(forecastServiceApi);
    }
}
