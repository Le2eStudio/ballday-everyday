package com.le2e.balldayeveryday.injection.module;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.data.remote.forecast.request.ApiContentHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ForecastModule.class)
public class DataModule {
    private final Application application;

    public DataModule(Application application){
        this.application = application;
    }

    @Provides
    @Singleton
    DataManager dataManager(ApiContentHelper apiContentHelper, SharedPreferences sharedPreferences){
        return new DataManager(apiContentHelper, sharedPreferences);
    }

    @Provides
    @Singleton
    SharedPreferences getSharedPreferences(){
        return PreferenceManager.getDefaultSharedPreferences(application);
    }
}
