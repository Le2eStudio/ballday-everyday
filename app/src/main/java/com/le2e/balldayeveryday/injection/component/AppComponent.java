package com.le2e.balldayeveryday.injection.component;

import com.le2e.balldayeveryday.injection.module.DataModule;
import com.le2e.balldayeveryday.ui.main.MainActivity;
import com.le2e.balldayeveryday.ui.main.fragments.current.CurrentFragment;
import com.le2e.balldayeveryday.ui.main.fragments.daily.DailyBreakdownFragment;
import com.le2e.balldayeveryday.ui.main.fragments.map.LocPickerFragment;
import com.le2e.balldayeveryday.ui.main.fragments.weekly.WeeklyBreakdownFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = DataModule.class)
public interface AppComponent {
    void inject(MainActivity mainActivity);

    void inject(CurrentFragment currentFragment);

    void inject(DailyBreakdownFragment dailyFragment);

    void inject(WeeklyBreakdownFragment weeklyFragment);

    void inject(LocPickerFragment locPickerFragment);
}
