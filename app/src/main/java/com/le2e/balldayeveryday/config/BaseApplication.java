package com.le2e.balldayeveryday.config;


import android.app.Application;
import android.content.Context;

import com.le2e.balldayeveryday.BuildConfig;
import com.le2e.balldayeveryday.injection.component.AppComponent;
import com.le2e.balldayeveryday.injection.component.DaggerAppComponent;
import com.le2e.balldayeveryday.injection.module.DataModule;
import com.le2e.balldayeveryday.injection.module.ForecastModule;
import com.le2e.balldayeveryday.injection.module.NetworkModule;

import timber.log.Timber;

public class BaseApplication extends Application {
    private static Context context;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        BaseApplication.context = getApplicationContext();

        appComponent = DaggerAppComponent.builder()
                .dataModule(new DataModule(this))
                .forecastModule(new ForecastModule())
                .networkModule(new NetworkModule())
                .build();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static BaseApplication get(){
        return (BaseApplication) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
