package com.le2e.balldayeveryday.ui.base;


import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import timber.log.Timber;

public class BaseLocationActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("trying google services");
        if (isGoogleServicesAvailable()) {
            initLocationServices();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationManager();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Timber.d("googleApiClient connected");
        startLocationManager();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Timber.w("Google APIs connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.e("Google APIs connection failed with code %d", connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        Timber.d("new loc: " + location.getLatitude() + " / " + location.getLongitude());
        setNewLocation(location);
    }

    private void initLocationServices() {
        Timber.d("connecting google api");
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        googleApiClient.connect();
    }

    protected boolean isGoogleServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int isAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            Dialog dialog = apiAvailability.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Can't connect to to play services", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    private void startLocationManager() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                Timber.d("Location not granted");
                // TODO decide if to load a screen with an address input to geocode or show a map to set the lat/lng to be used
                return;
            }
        }

        Timber.d("setting fused parameters");
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(50000);
        locationRequest.setFastestInterval(1000);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void stopLocationManager() {
        googleApiClient.disconnect();
    }

    protected void setNewLocation(Location location){
        // do nothing - allow each activity to implement this feature
    }

    protected void setupMapFragment(){
        // do nothing - allows an activity to add a map fragment if the service is available
    }
}
