package com.le2e.balldayeveryday.ui.load;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.le2e.balldayeveryday.R;
import com.le2e.balldayeveryday.ui.base.mvp.MvpBaseActivity;
import com.le2e.balldayeveryday.ui.main.MainActivity;

public class LoadActivity extends MvpBaseActivity<LoadView, LoadPresenter> {
    Button btn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);
        btn = (Button)findViewById(R.id.tempbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoadActivity.this, MainActivity.class));
            }
        });
    }

    @NonNull
    @Override
    public LoadPresenter createPresenter() {
        return new LoadPresenter();
    }

    @Override
    protected void setNewLocation(Location location) {
        if (location == null){
            // now location available - handle the error
        } else {
            // allow loading screen to finish and lead to the main activity
            // save that location?
        }
    }
}
