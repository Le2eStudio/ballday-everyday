package com.le2e.balldayeveryday.ui.main.fragments.daily;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.R;
import com.le2e.balldayeveryday.config.BaseApplication;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBaseFragment;

import javax.inject.Inject;

import timber.log.Timber;

public class DailyBreakdownFragment extends MvpBaseFragment<DailyFragmentView, DailyFragmentPresenter> implements DailyFragmentView {
    @Inject
    DataManager dataManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BaseApplication.get().getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_daily, container, false);
    }

    @NonNull
    @Override
    public DailyFragmentPresenter createPresenter() {
        return new DailyFragmentPresenter(dataManager);
    }
}
