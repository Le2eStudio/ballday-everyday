package com.le2e.balldayeveryday.ui.main;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBasePresenter;

public class MainPresenter extends MvpBasePresenter<MainView> {
    private DataManager dataManager;

    MainPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    void saveLatLng(Location location) {
        dataManager.saveLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
    }
}
