package com.le2e.balldayeveryday.ui.main.fragments.map;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.le2e.balldayeveryday.R;
import com.le2e.balldayeveryday.config.BaseApplication;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBaseFragment;

import javax.inject.Inject;

public class LocPickerFragment extends MvpBaseFragment<LocPickerView, LocPickerPresenter> implements OnMapReadyCallback {
    @Inject
    DataManager dataManager;

    SupportMapFragment mMapFragment;
    GoogleMap mMap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BaseApplication.get().getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);
        if (mMapFragment == null) {
            mMapFragment = SupportMapFragment.newInstance();
            getChildFragmentManager().beginTransaction().replace(R.id.map_fragment, mMapFragment).commit();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_loc_picker, container, false);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap == null) {
            mMapFragment.getMapAsync(this);
        }
    }

    @NonNull
    @Override
    public LocPickerPresenter createPresenter() {
        return new LocPickerPresenter(dataManager);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            mMap = googleMap;
        }
    }
}
