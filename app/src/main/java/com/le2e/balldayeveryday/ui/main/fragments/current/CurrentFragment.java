package com.le2e.balldayeveryday.ui.main.fragments.current;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.R;
import com.le2e.balldayeveryday.config.BaseApplication;
import com.le2e.balldayeveryday.data.local.CurrentlyDisplay;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.data.remote.forecast.response.Currently;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBaseFragment;

import javax.inject.Inject;

public class CurrentFragment extends MvpBaseFragment<CurrentFragmentView, CurrentFragmentPresenter> implements CurrentFragmentView {
    @Inject
    DataManager dataManager;

    private TextView tempTV;
    private TextView precipTV;
    private TextView summaryTV;
    private TextView humidityTV;
    private TextView pressureTV;
    private TextView windTV;
    private TextView visibilityTV;
    private TextView cloudsTV;
    private TextView timeTv;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        BaseApplication.get().getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_current, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timeTv = (TextView) view.findViewById(R.id.current_time);
        summaryTV = (TextView) view.findViewById(R.id.current_summary);
        tempTV = (TextView) view.findViewById(R.id.current_temp);
        precipTV = (TextView) view.findViewById(R.id.current_precip);
        humidityTV = (TextView) view.findViewById(R.id.current_humidity);
        pressureTV = (TextView) view.findViewById(R.id.current_pressure);
        windTV = (TextView) view.findViewById(R.id.current_wind);
        visibilityTV = (TextView) view.findViewById(R.id.current_visibility);
        cloudsTV = (TextView) view.findViewById(R.id.current_cloud);
    }

    @NonNull
    @Override
    public CurrentFragmentPresenter createPresenter() {
        return new CurrentFragmentPresenter(dataManager);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.displayCurrentWeather();
    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.removeSubscriptions();
    }

    @Override
    public void setCurrentDisplay(CurrentlyDisplay currently) {
        timeTv.setText(currently.getTime());
        summaryTV.setText(currently.getSummary());
        tempTV.setText(currently.getTemp());
        precipTV.setText(currently.getPrecip());
        humidityTV.setText(currently.getHumidity());
        pressureTV.setText(currently.getPressure());
        windTV.setText(currently.getWind());
        visibilityTV.setText(currently.getVisibility());
        cloudsTV.setText(currently.getCloud());
    }
}
