package com.le2e.balldayeveryday.ui.main.fragments.current;


import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.data.local.CurrentlyDisplay;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.data.remote.forecast.response.Currently;
import com.le2e.balldayeveryday.data.remote.forecast.response.ForecastResponse;
import com.le2e.balldayeveryday.data.utils.CurrentDataFormatter;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBasePresenter;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class CurrentFragmentPresenter extends MvpBasePresenter<CurrentFragmentView> {
    private DataManager dataManager;
    private Subscription forecastSub;
    private Currently savedCurrent;

    public CurrentFragmentPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public void removeSubscriptions(){
        if (forecastSub != null && !forecastSub.isUnsubscribed())
            forecastSub.unsubscribe();
    }

    public void displayCurrentWeather(){
        // get latlng from sharedpref
        LatLng latLng = dataManager.getLatLng();
        // handle is 0, 0
        if (latLng.latitude == 0 || latLng.longitude == 0){
            // handle this case
            Timber.d("Invalid location attempt to display");
        } else {
            callApi(latLng);
        }
    }

    private void callApi(LatLng latLng) {
        String location = latLng.latitude + ", " + latLng.longitude;
        Timber.d("Reporting weather for: " + location);
        forecastSub = dataManager.getForecastFromApi(location)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ForecastResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "ForecastAPI failure!");
                    }

                    @Override
                    public void onNext(ForecastResponse forecastResponse) {
                        Timber.d("ForecastAPI success!");
                        if (forecastResponse.getCurrently() != null) {
                            setCurrentlyDisplay(forecastResponse.getCurrently());
                            savedCurrent = forecastResponse.getCurrently();
                        }
                    }
                });
    }

    private void setCurrentlyDisplay(Currently currently) {
        if (isViewAttached()) {
            CurrentDataFormatter fmt = new CurrentDataFormatter();
            CurrentlyDisplay display = new CurrentlyDisplay();
            display.setTime(fmt.createDateDisplayString(currently.getTime(), null));
            display.setSummary(currently.getSummary());
            display.setTemp(fmt.createTempDisplayString(currently.getTemperature(), currently.getApparentTemperature()));
            display.setPrecip(fmt.createPrecipDisplayString(currently.getPrecipProbability(), currently.getPrecipType(), currently.getPrecipIntensity()));
            display.setHumidity(fmt.createHumidityDisplayString(currently.getHumidity()));
            display.setWind(fmt.createWindDisplayString(currently.getWindSpeed(), currently.getWindBearing()));
            display.setPressure(fmt.createPressureDisplayString(currently.getPressure()));
            display.setVisibility(fmt.createVisibilityDisplayString(currently.getVisibility()));
            display.setCloud(fmt.createCloudDisplayString(currently.getCloudCover()));

            getView().setCurrentDisplay(display);
        }
    }

    private LatLng getLatLng(){
        return dataManager.getLatLng();
    }
}
