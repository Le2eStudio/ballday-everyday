package com.le2e.balldayeveryday.ui.main.fragments.daily;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBasePresenter;

import rx.Subscription;

public class DailyFragmentPresenter extends MvpBasePresenter<DailyFragmentView> {
    private DataManager dataManager;
    private Subscription forecastSub;

    public DailyFragmentPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    public LatLng getLatLng(){
        return dataManager.getLatLng();
    }
}
