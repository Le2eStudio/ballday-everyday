package com.le2e.balldayeveryday.ui.base.mvp.core;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;

public abstract class MvpBaseFragment<V extends MvpView, P extends MvpPresenter<V>> extends Fragment implements MvpView {
    protected P presenter;

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        presenter = createPresenter();
        if(presenter == null){
            throw new NullPointerException("Presenter cannot be null");
        }

        presenter.attachView(getMvpView());
    }

    @NonNull
    public abstract P createPresenter();

    @NonNull
    public V getMvpView(){
        return (V) this;
    }

    @Override
    public void onDestroyView(){
        presenter.detachView();
        super.onDestroyView();
    }
}
