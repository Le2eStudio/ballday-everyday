package com.le2e.balldayeveryday.ui.main.fragments.weekly;


import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpBasePresenter;

public class WeeklyFragmentPresenter extends MvpBasePresenter<WeeklyFragmentView> {
    private DataManager dataManager;

    public WeeklyFragmentPresenter(DataManager dataManager){
        this.dataManager = dataManager;
    }
}
