package com.le2e.balldayeveryday.ui.main;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.le2e.balldayeveryday.R;
import com.le2e.balldayeveryday.config.BaseApplication;
import com.le2e.balldayeveryday.data.manager.DataManager;
import com.le2e.balldayeveryday.ui.base.mvp.MvpBaseActivity;
import com.le2e.balldayeveryday.ui.main.fragments.current.CurrentFragment;
import com.le2e.balldayeveryday.ui.main.fragments.daily.DailyBreakdownFragment;
import com.le2e.balldayeveryday.ui.main.fragments.map.LocPickerFragment;
import com.le2e.balldayeveryday.ui.main.fragments.weekly.WeeklyBreakdownFragment;

import javax.inject.Inject;

public class MainActivity extends MvpBaseActivity<MainView, MainPresenter>
        implements NavigationView.OnNavigationItemSelectedListener {

    @Inject
    DataManager dataManager;

    private FragmentManager fm;
    private NavigationView navigationView;

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter(dataManager);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("Tits", "base");
        BaseApplication.get().getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_current);

        fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.content_frame, new CurrentFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (currentFragment instanceof CurrentFragment)
                navigationView.setCheckedItem(R.id.nav_current);
            if (currentFragment instanceof DailyBreakdownFragment)
                navigationView.setCheckedItem(R.id.nav_daily);
            if (currentFragment instanceof WeeklyBreakdownFragment)
                navigationView.setCheckedItem(R.id.nav_weekly);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_overflow_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (id == R.id.nav_current) {
            if (!(currentFragment instanceof CurrentFragment))
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
                        .replace(R.id.content_frame, new CurrentFragment())
                        .addToBackStack("current")
                        .commit();
        } else if (id == R.id.nav_daily) {
            if (!(currentFragment instanceof DailyBreakdownFragment))
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
                        .replace(R.id.content_frame, new DailyBreakdownFragment())
                        .addToBackStack("daily")
                        .commit();
        } else if (id == R.id.nav_weekly) {
            if (!(currentFragment instanceof WeeklyBreakdownFragment))
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
                        .replace(R.id.content_frame, new WeeklyBreakdownFragment())
                        .addToBackStack("weekly")
                        .commit();
        } else if (id == R.id.nav_loc_pick) {
            if (!(currentFragment instanceof LocPickerFragment)) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_bottom)
                                .replace(R.id.content_frame, new LocPickerFragment())
                                .addToBackStack("loc")
                                .commit();
                    }
                }, 410);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void setNewLocation(Location location) {
        presenter.saveLatLng(location);
    }
}
