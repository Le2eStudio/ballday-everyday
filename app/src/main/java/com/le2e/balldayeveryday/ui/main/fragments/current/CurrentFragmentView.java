package com.le2e.balldayeveryday.ui.main.fragments.current;


import com.le2e.balldayeveryday.data.local.CurrentlyDisplay;
import com.le2e.balldayeveryday.data.remote.forecast.response.Currently;
import com.le2e.balldayeveryday.ui.base.mvp.core.MvpView;

public interface CurrentFragmentView extends MvpView {
    void setCurrentDisplay(CurrentlyDisplay currently);
}
